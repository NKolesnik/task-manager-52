package ru.t1consulting.nkolesnik.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.event.OperationEvent;
import ru.t1consulting.nkolesnik.tm.enumerated.EntityOperationType;

import javax.persistence.*;

import java.util.function.Consumer;

import static ru.t1consulting.nkolesnik.tm.enumerated.EntityOperationType.*;

@NoArgsConstructor
public class EntityListener {

    @Nullable
    private static Consumer<OperationEvent> CONSUMER = null;

    public static Consumer<OperationEvent> getConsumer(){
        return CONSUMER;
    }

    public static void setConsumer(@NotNull final Consumer<OperationEvent> consumer) {
        EntityListener.CONSUMER = consumer;
    }

    @SneakyThrows
    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        if(CONSUMER == null) return;
        CONSUMER.accept(new OperationEvent(operationType, entity));
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

}

