package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.event.OperationEvent;

public interface ISenderService {

    void send(@NotNull String message);

}
